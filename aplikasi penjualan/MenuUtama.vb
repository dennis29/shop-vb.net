

Public Class MenuUtama


    Private Sub KasirToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles KasirToolStripMenuItem.Click
        Petugas.Show()
    End Sub

    Private Sub BarangToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BarangToolStripMenuItem.Click
        Barang.Show()
    End Sub

    Private Sub PenjualanToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PenjualanToolStripMenuItem.Click
        Penjualan.Show()
    End Sub

    Private Sub DataMasterToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DataMasterToolStripMenuItem.Click
        LaporanMaster.Show()
    End Sub

    Private Sub DataPenjualanToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DataPenjualanToolStripMenuItem.Click
        LaporanPenjualan.Show()
    End Sub

    Private Sub GantiPasswordUserToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GantiPasswordUserToolStripMenuItem.Click
        GantiPassword.Show()
    End Sub


    Private Sub TutupToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TutupToolStripMenuItem.Click
        If MessageBox.Show("Tutup Palikasi...?", "", MessageBoxButtons.YesNo) = Windows.Forms.DialogResult.Yes Then
            Conn.Close()
            End
        End If
    End Sub

    Private Sub MenuUtama_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Conn.Close()
        End
    End Sub

    Private Sub MenuUtama_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If e.KeyChar = Chr(27) Then
            If MessageBox.Show("Tutup Palikasi...?", "", MessageBoxButtons.YesNo) = Windows.Forms.DialogResult.Yes Then
                End
            End If
        End If
    End Sub

    Private Sub RincianPenjualanToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RincianPenjualanToolStripMenuItem.Click
        RincianTransaksi.Show()
    End Sub

    Private Sub ReturPenjualanToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ReturPenjualanToolStripMenuItem.Click
        Retur.Show()
    End Sub

    'Private Sub FileToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles FileToolStripMenuItem.Click

    'End Sub

    'Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click
    '    Barang.ShowDialog()
    'End Sub

    'Private Sub Button11_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button11.Click
    '    Retur.Show()
    'End Sub

    'Private Sub Button16_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button16.Click
    '    LaporanMaster.Show()
    'End Sub

    'Private Sub Button15_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button15.Click
    '    LaporanPenjualan.Show()
    'End Sub

    'Private Sub Button14_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button14.Click
    '    RincianTransaksi.Show()
    'End Sub

    'Private Sub Button9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button9.Click
    '    GantiPassword.ShowDialog()
    'End Sub

    'Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
    '    BackupData.ShowDialog()
    'End Sub

    'Private Sub MenuUtama_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
    '    GBMaster.Visible = True
    '    GBMaster.Dock = DockStyle.Bottom
    '    GBTRansaksi.Visible = False
    '    GBLaporan.Visible = False
    '    GBUtil.Visible = False
    'End Sub



    'Private Sub Button8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button8.Click
    '    Petugas.ShowDialog()
    'End Sub

    'Private Sub Button12_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button12.Click
    '    Penjualan.Show()
    'End Sub



    'Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click

    'End Sub

    'Private Sub Button2_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
    '    GBMaster.Visible = False
    '    GBTRansaksi.Visible = True
    '    GBTRansaksi.Dock = DockStyle.Bottom
    '    GBLaporan.Visible = False
    '    GBUtil.Visible = False
    'End Sub

    'Private Sub Button3_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
    '    GBMaster.Visible = False
    '    GBTRansaksi.Visible = False
    '    GBLaporan.Visible = True
    '    GBLaporan.Dock = DockStyle.Bottom
    '    GBUtil.Visible = False
    'End Sub

    'Private Sub Button4_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
    '    GBMaster.Visible = False
    '    GBTRansaksi.Visible = False
    '    GBLaporan.Visible = False
    '    GBUtil.Visible = True
    '    GBUtil.Dock = DockStyle.Bottom
    'End Sub

    Private Sub BackpDatabaseToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BackpDatabaseToolStripMenuItem.Click
        BackupData.ShowDialog()
    End Sub
End Class