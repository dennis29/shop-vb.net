Imports System.Data.OleDb

Public Class Barang
    Sub Kosongkan()
        TKode.Text = ""
        TNama.Text = ""
        TBeli.Text = ""
        TJual.Text = ""
        TJumlah.Text = ""
        CmbSatuan.Text = ""
        TKode.Focus()
    End Sub

    Sub DataBaru()
        TNama.Text = ""
        TBeli.Text = ""
        TJual.Text = ""
        TJumlah.Text = ""
        CmbSatuan.Text = ""
        CmbSatuan.Focus()
    End Sub

    Sub Tampilkan()
        da = New OleDbDataAdapter("Select * from barang", Conn)
        ds = New DataSet
        ds.Clear()
        da.Fill(ds, "Barang")
        DGV.DataSource = (ds.Tables("Barang"))
        DGV.ReadOnly = True
    End Sub

    Sub TampilSatuan()
        CmbSatuan.Items.Clear()
        cmd = New OleDbCommand("select distinct satuan from Barang", Conn)
        rd = cmd.ExecuteReader
        While rd.Read
            CmbSatuan.Items.Add(rd.GetString(0))
        End While
    End Sub

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call Koneksi()
        Call Tampilkan()
        Call TampilSatuan()
    End Sub

    Private Sub TKode_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TKode.KeyPress
        If e.KeyChar = Chr(13) Then
            cmd = New OleDbCommand("select * from barang where kode_barang='" & TKode.Text & "'", Conn)
            rd = cmd.ExecuteReader
            rd.Read()
            If rd.HasRows = True Then
                TNama.Text = rd.GetString(1)
                TBeli.Text = rd.GetValue(2)
                TJual.Text = rd.GetValue(3)
                TJumlah.Text = rd.GetValue(4)
                CmbSatuan.Text = rd.GetString(5)
                TNama.Focus()
            Else
                Call DataBaru()
                TNama.Focus()
            End If
        End If
        If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack) Then e.Handled() = True
    End Sub

    Private Sub TNama_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TNama.KeyPress
        If e.KeyChar = Chr(13) Then TBeli.Focus()
    End Sub

    Private Sub TBeli_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TBeli.KeyPress
        If e.KeyChar = Chr(13) Then TJual.Focus()
        If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack) Then e.Handled() = True
    End Sub

    Private Sub TJual_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TJual.KeyPress
        If e.KeyChar = Chr(13) Then TJumlah.Focus()
        If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack) Then e.Handled() = True
    End Sub

    Private Sub TJumlah_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TJumlah.KeyPress
        If e.KeyChar = Chr(13) Then CmbSatuan.Focus()
        If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack) Then e.Handled() = True
    End Sub

    Private Sub cmbsatuan_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles CmbSatuan.KeyPress
        If e.KeyChar = Chr(13) Then CmdSimpan.Focus()
    End Sub

    Private Sub CmdSimpan_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CmdSimpan.Click
        If TKode.Text = "" Or TNama.Text = "" Or TBeli.Text = "" Or TJual.Text = "" Or TJumlah.Text = "" Or CmbSatuan.Text = "" Then
            MsgBox("Data Belum Lengkap")
            Exit Sub
        Else
            cmd = New OleDbCommand("Select * from barang where kode_barang='" & TKode.Text & "'", Conn)
            rd = cmd.ExecuteReader
            rd.Read()
            If Not rd.HasRows Then
                Dim sqltambah As String = "Insert into barang(kode_barang,nama_barang,harga_beli,harga_jual,jumlah_barang,satuan) values " & _
                "('" & TKode.Text & "','" & TNama.Text & "','" & TBeli.Text & "','" & TJual.Text & "','" & TJumlah.Text & "','" & CmbSatuan.Text & "')"
                cmd = New OleDbCommand(sqltambah, Conn)
                cmd.ExecuteNonQuery()
                Call Kosongkan()
                Call Tampilkan()
            Else
                Dim sqledit As String = "Update barang set " & _
                "Nama_Barang='" & TNama.Text & "', " & _
                "Harga_beli='" & TBeli.Text & "', " & _
                "Harga_jual='" & TJual.Text & "', " & _
                "Jumlah_Barang='" & TJumlah.Text & "', " & _
                "Satuan='" & CmbSatuan.Text & "' " & _
                "where kode_barang='" & TKode.Text & "'"
                cmd = New OleDbCommand(sqledit, Conn)
                cmd.ExecuteNonQuery()
                Call Kosongkan()
                Call Tampilkan()
            End If
        End If


    End Sub

    Private Sub CmdBatal_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CmdBatal.Click
        Call kosongkan()
    End Sub

    Private Sub CmdTutup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CmdTutup.Click
        Me.Close()
    End Sub

    Private Sub CmdHapus_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CmdHapus.Click
        If TKode.Text = "" Then
            MsgBox("Isi kode barang terlebih dahulu")
            TKode.Focus()
            Exit Sub
        Else
            If MessageBox.Show("Yakin akan dihapus..?", "", MessageBoxButtons.YesNo) = Windows.Forms.DialogResult.Yes Then
                cmd = New OleDbCommand("Delete * from barang where kode_barang='" & TKode.Text & "'", Conn)
                cmd.ExecuteNonQuery()
                Call Kosongkan()
                Call Tampilkan()
            Else
                Call Kosongkan()
            End If
        End If
    End Sub

    Private Sub TextBox1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox1.TextChanged
        da = New OleDbDataAdapter("select * from Barang where nama_Barang like '%" & TextBox1.Text & "%'", Conn)
        ds = New DataSet
        da.Fill(ds, "ketemu")
        DGV.DataSource = ds.Tables("ketemu")
        DGV.ReadOnly = True
    End Sub
End Class
