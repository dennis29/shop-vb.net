<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MasterMenu
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(MasterMenu))
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip
        Me.MasterToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.PetugasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.BarangToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.TransaksiToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.PenjualanToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ReturPenjualanToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.LaporanToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.DataMasterToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.DataTransaksiToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.RincianPenjualanToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.UtilityToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.GantiPassswordToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.BackupDatabaseToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ManualBookToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.TutupAplikasiToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip
        Me.Panel1 = New System.Windows.Forms.ToolStripStatusLabel
        Me.Panel2 = New System.Windows.Forms.ToolStripStatusLabel
        Me.Panel3 = New System.Windows.Forms.ToolStripStatusLabel
        Me.GB5 = New System.Windows.Forms.GroupBox
        Me.Button25 = New System.Windows.Forms.Button
        Me.Button26 = New System.Windows.Forms.Button
        Me.Button27 = New System.Windows.Forms.Button
        Me.GB4 = New System.Windows.Forms.GroupBox
        Me.Button18 = New System.Windows.Forms.Button
        Me.Button19 = New System.Windows.Forms.Button
        Me.Button21 = New System.Windows.Forms.Button
        Me.GB3 = New System.Windows.Forms.GroupBox
        Me.Button10 = New System.Windows.Forms.Button
        Me.Button11 = New System.Windows.Forms.Button
        Me.GB2 = New System.Windows.Forms.GroupBox
        Me.Button7 = New System.Windows.Forms.Button
        Me.Button6 = New System.Windows.Forms.Button
        Me.GB1 = New System.Windows.Forms.GroupBox
        Me.LinkLabel1 = New System.Windows.Forms.LinkLabel
        Me.Label1 = New System.Windows.Forms.Label
        Me.Button5 = New System.Windows.Forms.Button
        Me.Button4 = New System.Windows.Forms.Button
        Me.Button3 = New System.Windows.Forms.Button
        Me.Button2 = New System.Windows.Forms.Button
        Me.Button1 = New System.Windows.Forms.Button
        Me.MenuStrip1.SuspendLayout()
        Me.StatusStrip1.SuspendLayout()
        Me.GB5.SuspendLayout()
        Me.GB4.SuspendLayout()
        Me.GB3.SuspendLayout()
        Me.GB2.SuspendLayout()
        Me.GB1.SuspendLayout()
        Me.SuspendLayout()
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.MasterToolStripMenuItem, Me.TransaksiToolStripMenuItem, Me.LaporanToolStripMenuItem, Me.UtilityToolStripMenuItem, Me.TutupAplikasiToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(755, 24)
        Me.MenuStrip1.TabIndex = 0
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'MasterToolStripMenuItem
        '
        Me.MasterToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.PetugasToolStripMenuItem, Me.BarangToolStripMenuItem})
        Me.MasterToolStripMenuItem.Name = "MasterToolStripMenuItem"
        Me.MasterToolStripMenuItem.Size = New System.Drawing.Size(52, 20)
        Me.MasterToolStripMenuItem.Text = "Master"
        '
        'PetugasToolStripMenuItem
        '
        Me.PetugasToolStripMenuItem.Name = "PetugasToolStripMenuItem"
        Me.PetugasToolStripMenuItem.Size = New System.Drawing.Size(124, 22)
        Me.PetugasToolStripMenuItem.Text = "Petugas"
        '
        'BarangToolStripMenuItem
        '
        Me.BarangToolStripMenuItem.Name = "BarangToolStripMenuItem"
        Me.BarangToolStripMenuItem.Size = New System.Drawing.Size(124, 22)
        Me.BarangToolStripMenuItem.Text = "Barang"
        '
        'TransaksiToolStripMenuItem
        '
        Me.TransaksiToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.PenjualanToolStripMenuItem, Me.ReturPenjualanToolStripMenuItem})
        Me.TransaksiToolStripMenuItem.Name = "TransaksiToolStripMenuItem"
        Me.TransaksiToolStripMenuItem.Size = New System.Drawing.Size(64, 20)
        Me.TransaksiToolStripMenuItem.Text = "Transaksi"
        '
        'PenjualanToolStripMenuItem
        '
        Me.PenjualanToolStripMenuItem.Name = "PenjualanToolStripMenuItem"
        Me.PenjualanToolStripMenuItem.Size = New System.Drawing.Size(162, 22)
        Me.PenjualanToolStripMenuItem.Text = "Penjualan"
        '
        'ReturPenjualanToolStripMenuItem
        '
        Me.ReturPenjualanToolStripMenuItem.Name = "ReturPenjualanToolStripMenuItem"
        Me.ReturPenjualanToolStripMenuItem.Size = New System.Drawing.Size(162, 22)
        Me.ReturPenjualanToolStripMenuItem.Text = "Retur Penjualan"
        '
        'LaporanToolStripMenuItem
        '
        Me.LaporanToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.DataMasterToolStripMenuItem, Me.DataTransaksiToolStripMenuItem, Me.RincianPenjualanToolStripMenuItem})
        Me.LaporanToolStripMenuItem.Name = "LaporanToolStripMenuItem"
        Me.LaporanToolStripMenuItem.Size = New System.Drawing.Size(58, 20)
        Me.LaporanToolStripMenuItem.Text = "Laporan"
        '
        'DataMasterToolStripMenuItem
        '
        Me.DataMasterToolStripMenuItem.Name = "DataMasterToolStripMenuItem"
        Me.DataMasterToolStripMenuItem.Size = New System.Drawing.Size(169, 22)
        Me.DataMasterToolStripMenuItem.Text = "Data Master"
        '
        'DataTransaksiToolStripMenuItem
        '
        Me.DataTransaksiToolStripMenuItem.Name = "DataTransaksiToolStripMenuItem"
        Me.DataTransaksiToolStripMenuItem.Size = New System.Drawing.Size(169, 22)
        Me.DataTransaksiToolStripMenuItem.Text = "Data Transaksi"
        '
        'RincianPenjualanToolStripMenuItem
        '
        Me.RincianPenjualanToolStripMenuItem.Name = "RincianPenjualanToolStripMenuItem"
        Me.RincianPenjualanToolStripMenuItem.Size = New System.Drawing.Size(169, 22)
        Me.RincianPenjualanToolStripMenuItem.Text = "Rincian Penjualan"
        '
        'UtilityToolStripMenuItem
        '
        Me.UtilityToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.GantiPassswordToolStripMenuItem, Me.BackupDatabaseToolStripMenuItem, Me.ManualBookToolStripMenuItem})
        Me.UtilityToolStripMenuItem.Name = "UtilityToolStripMenuItem"
        Me.UtilityToolStripMenuItem.Size = New System.Drawing.Size(46, 20)
        Me.UtilityToolStripMenuItem.Text = "Utility"
        '
        'GantiPassswordToolStripMenuItem
        '
        Me.GantiPassswordToolStripMenuItem.Name = "GantiPassswordToolStripMenuItem"
        Me.GantiPassswordToolStripMenuItem.Size = New System.Drawing.Size(168, 22)
        Me.GantiPassswordToolStripMenuItem.Text = "Ganti Passsword"
        '
        'BackupDatabaseToolStripMenuItem
        '
        Me.BackupDatabaseToolStripMenuItem.Name = "BackupDatabaseToolStripMenuItem"
        Me.BackupDatabaseToolStripMenuItem.Size = New System.Drawing.Size(168, 22)
        Me.BackupDatabaseToolStripMenuItem.Text = "Backup Database"
        '
        'ManualBookToolStripMenuItem
        '
        Me.ManualBookToolStripMenuItem.Name = "ManualBookToolStripMenuItem"
        Me.ManualBookToolStripMenuItem.Size = New System.Drawing.Size(168, 22)
        Me.ManualBookToolStripMenuItem.Text = "Manual Book"
        '
        'TutupAplikasiToolStripMenuItem
        '
        Me.TutupAplikasiToolStripMenuItem.Name = "TutupAplikasiToolStripMenuItem"
        Me.TutupAplikasiToolStripMenuItem.Size = New System.Drawing.Size(85, 20)
        Me.TutupAplikasiToolStripMenuItem.Text = "Tutup Aplikasi"
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Panel1, Me.Panel2, Me.Panel3})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 442)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(755, 22)
        Me.StatusStrip1.TabIndex = 1
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'Panel1
        '
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(0, 17)
        '
        'Panel2
        '
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(0, 17)
        '
        'Panel3
        '
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(0, 17)
        '
        'GB5
        '
        Me.GB5.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.GB5.Controls.Add(Me.Button25)
        Me.GB5.Controls.Add(Me.Button26)
        Me.GB5.Controls.Add(Me.Button27)
        Me.GB5.Dock = System.Windows.Forms.DockStyle.Top
        Me.GB5.Location = New System.Drawing.Point(0, 342)
        Me.GB5.Name = "GB5"
        Me.GB5.Size = New System.Drawing.Size(755, 80)
        Me.GB5.TabIndex = 7
        Me.GB5.TabStop = False
        Me.GB5.Text = "Utility"
        '
        'Button25
        '
        Me.Button25.FlatAppearance.BorderSize = 0
        Me.Button25.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button25.Image = CType(resources.GetObject("Button25.Image"), System.Drawing.Image)
        Me.Button25.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.Button25.Location = New System.Drawing.Point(197, 13)
        Me.Button25.Name = "Button25"
        Me.Button25.Size = New System.Drawing.Size(81, 67)
        Me.Button25.TabIndex = 13
        Me.Button25.Text = "Manual Book"
        Me.Button25.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.Button25.UseVisualStyleBackColor = True
        '
        'Button26
        '
        Me.Button26.FlatAppearance.BorderSize = 0
        Me.Button26.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button26.Image = CType(resources.GetObject("Button26.Image"), System.Drawing.Image)
        Me.Button26.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.Button26.Location = New System.Drawing.Point(95, 13)
        Me.Button26.Name = "Button26"
        Me.Button26.Size = New System.Drawing.Size(103, 67)
        Me.Button26.TabIndex = 12
        Me.Button26.Text = "Backup Database"
        Me.Button26.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.Button26.UseVisualStyleBackColor = True
        '
        'Button27
        '
        Me.Button27.FlatAppearance.BorderSize = 0
        Me.Button27.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button27.Image = CType(resources.GetObject("Button27.Image"), System.Drawing.Image)
        Me.Button27.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.Button27.Location = New System.Drawing.Point(6, 13)
        Me.Button27.Name = "Button27"
        Me.Button27.Size = New System.Drawing.Size(92, 67)
        Me.Button27.TabIndex = 11
        Me.Button27.Text = "Ganti Password"
        Me.Button27.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.Button27.UseVisualStyleBackColor = True
        '
        'GB4
        '
        Me.GB4.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.GB4.Controls.Add(Me.Button18)
        Me.GB4.Controls.Add(Me.Button19)
        Me.GB4.Controls.Add(Me.Button21)
        Me.GB4.Dock = System.Windows.Forms.DockStyle.Top
        Me.GB4.Location = New System.Drawing.Point(0, 262)
        Me.GB4.Name = "GB4"
        Me.GB4.Size = New System.Drawing.Size(755, 80)
        Me.GB4.TabIndex = 6
        Me.GB4.TabStop = False
        Me.GB4.Text = "Laporan"
        '
        'Button18
        '
        Me.Button18.FlatAppearance.BorderSize = 0
        Me.Button18.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button18.Image = CType(resources.GetObject("Button18.Image"), System.Drawing.Image)
        Me.Button18.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.Button18.Location = New System.Drawing.Point(148, 14)
        Me.Button18.Name = "Button18"
        Me.Button18.Size = New System.Drawing.Size(72, 60)
        Me.Button18.TabIndex = 14
        Me.Button18.Text = "Rincian Transaksi"
        Me.Button18.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.Button18.UseVisualStyleBackColor = True
        '
        'Button19
        '
        Me.Button19.FlatAppearance.BorderSize = 0
        Me.Button19.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button19.Image = CType(resources.GetObject("Button19.Image"), System.Drawing.Image)
        Me.Button19.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.Button19.Location = New System.Drawing.Point(77, 14)
        Me.Button19.Name = "Button19"
        Me.Button19.Size = New System.Drawing.Size(65, 60)
        Me.Button19.TabIndex = 13
        Me.Button19.Text = "Penjualan"
        Me.Button19.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.Button19.UseVisualStyleBackColor = True
        '
        'Button21
        '
        Me.Button21.FlatAppearance.BorderSize = 0
        Me.Button21.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button21.Image = CType(resources.GetObject("Button21.Image"), System.Drawing.Image)
        Me.Button21.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.Button21.Location = New System.Drawing.Point(6, 13)
        Me.Button21.Name = "Button21"
        Me.Button21.Size = New System.Drawing.Size(65, 60)
        Me.Button21.TabIndex = 11
        Me.Button21.Text = "Master"
        Me.Button21.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.Button21.UseVisualStyleBackColor = True
        '
        'GB3
        '
        Me.GB3.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.GB3.Controls.Add(Me.Button10)
        Me.GB3.Controls.Add(Me.Button11)
        Me.GB3.Dock = System.Windows.Forms.DockStyle.Top
        Me.GB3.Location = New System.Drawing.Point(0, 182)
        Me.GB3.Name = "GB3"
        Me.GB3.Size = New System.Drawing.Size(755, 80)
        Me.GB3.TabIndex = 5
        Me.GB3.TabStop = False
        Me.GB3.Text = "Transaksi"
        '
        'Button10
        '
        Me.Button10.FlatAppearance.BorderSize = 0
        Me.Button10.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button10.Image = CType(resources.GetObject("Button10.Image"), System.Drawing.Image)
        Me.Button10.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.Button10.Location = New System.Drawing.Point(83, 19)
        Me.Button10.Name = "Button10"
        Me.Button10.Size = New System.Drawing.Size(63, 61)
        Me.Button10.TabIndex = 8
        Me.Button10.Text = "Retur Jual"
        Me.Button10.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.Button10.UseVisualStyleBackColor = True
        '
        'Button11
        '
        Me.Button11.FlatAppearance.BorderSize = 0
        Me.Button11.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button11.Image = CType(resources.GetObject("Button11.Image"), System.Drawing.Image)
        Me.Button11.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.Button11.Location = New System.Drawing.Point(12, 19)
        Me.Button11.Name = "Button11"
        Me.Button11.Size = New System.Drawing.Size(65, 60)
        Me.Button11.TabIndex = 7
        Me.Button11.Text = "Penjualan"
        Me.Button11.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.Button11.UseVisualStyleBackColor = True
        '
        'GB2
        '
        Me.GB2.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.GB2.Controls.Add(Me.Button7)
        Me.GB2.Controls.Add(Me.Button6)
        Me.GB2.Dock = System.Windows.Forms.DockStyle.Top
        Me.GB2.Location = New System.Drawing.Point(0, 103)
        Me.GB2.Name = "GB2"
        Me.GB2.Size = New System.Drawing.Size(755, 79)
        Me.GB2.TabIndex = 4
        Me.GB2.TabStop = False
        Me.GB2.Text = "Master"
        '
        'Button7
        '
        Me.Button7.FlatAppearance.BorderSize = 0
        Me.Button7.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button7.Image = CType(resources.GetObject("Button7.Image"), System.Drawing.Image)
        Me.Button7.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.Button7.Location = New System.Drawing.Point(77, 16)
        Me.Button7.Name = "Button7"
        Me.Button7.Size = New System.Drawing.Size(65, 60)
        Me.Button7.TabIndex = 2
        Me.Button7.Text = "Barang"
        Me.Button7.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.Button7.UseVisualStyleBackColor = True
        '
        'Button6
        '
        Me.Button6.FlatAppearance.BorderSize = 0
        Me.Button6.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button6.Image = CType(resources.GetObject("Button6.Image"), System.Drawing.Image)
        Me.Button6.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.Button6.Location = New System.Drawing.Point(6, 16)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(65, 60)
        Me.Button6.TabIndex = 1
        Me.Button6.Text = "Petugas"
        Me.Button6.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.Button6.UseVisualStyleBackColor = True
        '
        'GB1
        '
        Me.GB1.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.GB1.Controls.Add(Me.LinkLabel1)
        Me.GB1.Controls.Add(Me.Label1)
        Me.GB1.Controls.Add(Me.Button5)
        Me.GB1.Controls.Add(Me.Button4)
        Me.GB1.Controls.Add(Me.Button3)
        Me.GB1.Controls.Add(Me.Button2)
        Me.GB1.Controls.Add(Me.Button1)
        Me.GB1.Dock = System.Windows.Forms.DockStyle.Top
        Me.GB1.Location = New System.Drawing.Point(0, 24)
        Me.GB1.Name = "GB1"
        Me.GB1.Size = New System.Drawing.Size(755, 79)
        Me.GB1.TabIndex = 3
        Me.GB1.TabStop = False
        '
        'LinkLabel1
        '
        Me.LinkLabel1.AutoSize = True
        Me.LinkLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LinkLabel1.LinkVisited = True
        Me.LinkLabel1.Location = New System.Drawing.Point(369, 48)
        Me.LinkLabel1.Name = "LinkLabel1"
        Me.LinkLabel1.Size = New System.Drawing.Size(171, 16)
        Me.LinkLabel1.TabIndex = 6
        Me.LinkLabel1.TabStop = True
        Me.LinkLabel1.Text = "http://www.konsutasivb.com"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(368, 25)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(122, 16)
        Me.Label1.TabIndex = 5
        Me.Label1.Text = "Aplikasi Penjualan "
        '
        'Button5
        '
        Me.Button5.FlatAppearance.BorderSize = 0
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Image = CType(resources.GetObject("Button5.Image"), System.Drawing.Image)
        Me.Button5.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.Button5.Location = New System.Drawing.Point(287, 16)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(65, 60)
        Me.Button5.TabIndex = 4
        Me.Button5.Text = "Tutup"
        Me.Button5.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.Button5.UseVisualStyleBackColor = True
        '
        'Button4
        '
        Me.Button4.FlatAppearance.BorderSize = 0
        Me.Button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button4.Image = CType(resources.GetObject("Button4.Image"), System.Drawing.Image)
        Me.Button4.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.Button4.Location = New System.Drawing.Point(216, 16)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(65, 60)
        Me.Button4.TabIndex = 3
        Me.Button4.Text = "Utility"
        Me.Button4.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.Button4.UseVisualStyleBackColor = True
        '
        'Button3
        '
        Me.Button3.FlatAppearance.BorderSize = 0
        Me.Button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button3.Image = CType(resources.GetObject("Button3.Image"), System.Drawing.Image)
        Me.Button3.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.Button3.Location = New System.Drawing.Point(145, 16)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(65, 60)
        Me.Button3.TabIndex = 2
        Me.Button3.Text = "Laporan"
        Me.Button3.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.Button3.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.FlatAppearance.BorderSize = 0
        Me.Button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button2.Image = CType(resources.GetObject("Button2.Image"), System.Drawing.Image)
        Me.Button2.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.Button2.Location = New System.Drawing.Point(74, 16)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(65, 60)
        Me.Button2.TabIndex = 1
        Me.Button2.Text = "Transaksi"
        Me.Button2.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Button1
        '
        Me.Button1.FlatAppearance.BorderSize = 0
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Image = CType(resources.GetObject("Button1.Image"), System.Drawing.Image)
        Me.Button1.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.Button1.Location = New System.Drawing.Point(3, 16)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(65, 60)
        Me.Button1.TabIndex = 0
        Me.Button1.Text = "Master"
        Me.Button1.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.Button1.UseVisualStyleBackColor = True
        '
        'MasterMenu
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(755, 464)
        Me.Controls.Add(Me.GB5)
        Me.Controls.Add(Me.GB4)
        Me.Controls.Add(Me.GB3)
        Me.Controls.Add(Me.GB2)
        Me.Controls.Add(Me.GB1)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.MenuStrip1)
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Name = "MasterMenu"
        Me.Text = "MasterMenu"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.GB5.ResumeLayout(False)
        Me.GB4.ResumeLayout(False)
        Me.GB3.ResumeLayout(False)
        Me.GB2.ResumeLayout(False)
        Me.GB1.ResumeLayout(False)
        Me.GB1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents MasterToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents PetugasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents BarangToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TransaksiToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PenjualanToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ReturPenjualanToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents LaporanToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DataMasterToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DataTransaksiToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents UtilityToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents GantiPassswordToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents BackupDatabaseToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ManualBookToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RincianPenjualanToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TutupAplikasiToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Panel1 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents Panel2 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents Panel3 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents GB5 As System.Windows.Forms.GroupBox
    Friend WithEvents Button25 As System.Windows.Forms.Button
    Friend WithEvents Button26 As System.Windows.Forms.Button
    Friend WithEvents Button27 As System.Windows.Forms.Button
    Friend WithEvents GB4 As System.Windows.Forms.GroupBox
    Friend WithEvents Button18 As System.Windows.Forms.Button
    Friend WithEvents Button19 As System.Windows.Forms.Button
    Friend WithEvents Button21 As System.Windows.Forms.Button
    Friend WithEvents GB3 As System.Windows.Forms.GroupBox
    Friend WithEvents Button10 As System.Windows.Forms.Button
    Friend WithEvents Button11 As System.Windows.Forms.Button
    Friend WithEvents GB2 As System.Windows.Forms.GroupBox
    Friend WithEvents Button7 As System.Windows.Forms.Button
    Friend WithEvents Button6 As System.Windows.Forms.Button
    Friend WithEvents GB1 As System.Windows.Forms.GroupBox
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents LinkLabel1 As System.Windows.Forms.LinkLabel
    Friend WithEvents Label1 As System.Windows.Forms.Label
End Class
