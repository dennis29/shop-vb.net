<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Penjualan
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Penjualan))
        Me.Label1 = New System.Windows.Forms.Label
        Me.Faktur = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.Tanggal = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.Jam = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.Total = New System.Windows.Forms.Label
        Me.Label9 = New System.Windows.Forms.Label
        Me.Label10 = New System.Windows.Forms.Label
        Me.Kembali = New System.Windows.Forms.Label
        Me.Label13 = New System.Windows.Forms.Label
        Me.Item = New System.Windows.Forms.Label
        Me.BTNSimpan = New System.Windows.Forms.Button
        Me.BTNBatal = New System.Windows.Forms.Button
        Me.BTNTutup = New System.Windows.Forms.Button
        Me.Dibayar = New System.Windows.Forms.TextBox
        Me.DGV = New System.Windows.Forms.DataGridView
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.Label2 = New System.Windows.Forms.Label
        Me.Button1 = New System.Windows.Forms.Button
        CType(Me.DGV, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Label1.Location = New System.Drawing.Point(10, 59)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(45, 20)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Faktur"
        '
        'Faktur
        '
        Me.Faktur.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Faktur.Location = New System.Drawing.Point(61, 59)
        Me.Faktur.Name = "Faktur"
        Me.Faktur.Size = New System.Drawing.Size(100, 20)
        Me.Faktur.TabIndex = 1
        '
        'Label3
        '
        Me.Label3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Label3.Location = New System.Drawing.Point(171, 59)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(57, 20)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Tanggal"
        '
        'Tanggal
        '
        Me.Tanggal.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Tanggal.Location = New System.Drawing.Point(234, 59)
        Me.Tanggal.Name = "Tanggal"
        Me.Tanggal.Size = New System.Drawing.Size(100, 20)
        Me.Tanggal.TabIndex = 3
        '
        'Label5
        '
        Me.Label5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Label5.Location = New System.Drawing.Point(344, 59)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(40, 20)
        Me.Label5.TabIndex = 4
        Me.Label5.Text = "Jam"
        '
        'Jam
        '
        Me.Jam.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Jam.Location = New System.Drawing.Point(392, 59)
        Me.Jam.Name = "Jam"
        Me.Jam.Size = New System.Drawing.Size(100, 20)
        Me.Jam.TabIndex = 5
        '
        'Label7
        '
        Me.Label7.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Label7.Location = New System.Drawing.Point(370, 336)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(54, 20)
        Me.Label7.TabIndex = 11
        Me.Label7.Text = "Total"
        '
        'Total
        '
        Me.Total.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Total.Location = New System.Drawing.Point(432, 336)
        Me.Total.Name = "Total"
        Me.Total.Size = New System.Drawing.Size(100, 20)
        Me.Total.TabIndex = 10
        Me.Total.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label9
        '
        Me.Label9.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Label9.Location = New System.Drawing.Point(370, 361)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(54, 20)
        Me.Label9.TabIndex = 9
        Me.Label9.Text = "Dibayar"
        '
        'Label10
        '
        Me.Label10.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Label10.Location = New System.Drawing.Point(370, 382)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(54, 20)
        Me.Label10.TabIndex = 8
        Me.Label10.Text = "Kembali"
        '
        'Kembali
        '
        Me.Kembali.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Kembali.Location = New System.Drawing.Point(432, 382)
        Me.Kembali.Name = "Kembali"
        Me.Kembali.Size = New System.Drawing.Size(100, 20)
        Me.Kembali.TabIndex = 7
        Me.Kembali.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label13
        '
        Me.Label13.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Label13.Location = New System.Drawing.Point(256, 336)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(42, 20)
        Me.Label13.TabIndex = 13
        Me.Label13.Text = "Item"
        '
        'Item
        '
        Me.Item.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Item.Location = New System.Drawing.Point(304, 336)
        Me.Item.Name = "Item"
        Me.Item.Size = New System.Drawing.Size(48, 20)
        Me.Item.TabIndex = 12
        Me.Item.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'BTNSimpan
        '
        Me.BTNSimpan.Location = New System.Drawing.Point(13, 336)
        Me.BTNSimpan.Name = "BTNSimpan"
        Me.BTNSimpan.Size = New System.Drawing.Size(75, 25)
        Me.BTNSimpan.TabIndex = 14
        Me.BTNSimpan.Text = "Simpan"
        Me.BTNSimpan.UseVisualStyleBackColor = True
        '
        'BTNBatal
        '
        Me.BTNBatal.Location = New System.Drawing.Point(94, 336)
        Me.BTNBatal.Name = "BTNBatal"
        Me.BTNBatal.Size = New System.Drawing.Size(75, 25)
        Me.BTNBatal.TabIndex = 15
        Me.BTNBatal.Text = "Batal"
        Me.BTNBatal.UseVisualStyleBackColor = True
        '
        'BTNTutup
        '
        Me.BTNTutup.Location = New System.Drawing.Point(175, 336)
        Me.BTNTutup.Name = "BTNTutup"
        Me.BTNTutup.Size = New System.Drawing.Size(75, 25)
        Me.BTNTutup.TabIndex = 16
        Me.BTNTutup.Text = "Tutup"
        Me.BTNTutup.UseVisualStyleBackColor = True
        '
        'Dibayar
        '
        Me.Dibayar.Location = New System.Drawing.Point(432, 359)
        Me.Dibayar.Name = "Dibayar"
        Me.Dibayar.Size = New System.Drawing.Size(100, 20)
        Me.Dibayar.TabIndex = 17
        Me.Dibayar.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'DGV
        '
        Me.DGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGV.Location = New System.Drawing.Point(13, 91)
        Me.DGV.Name = "DGV"
        Me.DGV.Size = New System.Drawing.Size(519, 227)
        Me.DGV.TabIndex = 0
        '
        'Timer1
        '
        Me.Timer1.Enabled = True
        '
        'Label2
        '
        Me.Label2.Dock = System.Windows.Forms.DockStyle.Top
        Me.Label2.Font = New System.Drawing.Font("Monotype Corsiva", 20.25!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.White
        Me.Label2.Image = CType(resources.GetObject("Label2.Image"), System.Drawing.Image)
        Me.Label2.Location = New System.Drawing.Point(0, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(542, 50)
        Me.Label2.TabIndex = 35
        Me.Label2.Text = "Transaksi Penjualan"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(16, 373)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(153, 28)
        Me.Button1.TabIndex = 36
        Me.Button1.Text = "Kode Barang"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Penjualan
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(542, 412)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.DGV)
        Me.Controls.Add(Me.Dibayar)
        Me.Controls.Add(Me.BTNTutup)
        Me.Controls.Add(Me.BTNBatal)
        Me.Controls.Add(Me.BTNSimpan)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.Item)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Total)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.Kembali)
        Me.Controls.Add(Me.Jam)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Tanggal)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Faktur)
        Me.Controls.Add(Me.Label1)
        Me.Name = "Penjualan"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        CType(Me.DGV, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Faktur As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Tanggal As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Jam As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Total As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Kembali As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Item As System.Windows.Forms.Label
    Friend WithEvents BTNSimpan As System.Windows.Forms.Button
    Friend WithEvents BTNBatal As System.Windows.Forms.Button
    Friend WithEvents BTNTutup As System.Windows.Forms.Button
    Friend WithEvents Dibayar As System.Windows.Forms.TextBox
    Friend WithEvents DGV As System.Windows.Forms.DataGridView
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Button1 As System.Windows.Forms.Button
End Class
